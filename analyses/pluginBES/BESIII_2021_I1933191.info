Name: BESIII_2021_I1933191
Year: 2021
Summary: Cross section for $e^+e^-\to \psi(2S)\pi^+\pi^-$ at energies between 4.0 and 4.7 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1933191
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 104 (2021) 5, 052012
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Description:
  'Measurement of the cross section for $e^+e^-\to \psi(2S)\pi^+\pi^-$ at energies between 4.0 and 4.7 GeV by the BES collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021njb
BibTeX: '@article{BESIII:2021njb,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Cross section measurement of $e^+e^-\rightarrow\pi^+\pi^-(3686)$ from $\sqrt{S}=4.0076$ to 4.6984~GeV}",
    eprint = "2107.09210",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.104.052012",
    journal = "Phys. Rev. D",
    volume = "104",
    number = "5",
    pages = "052012",
    year = "2021"
}'
