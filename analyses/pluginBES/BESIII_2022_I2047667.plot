BEGIN PLOT /BESIII_2022_I2047667/d01-x01-y01
Title=Cross section for $e^+e^-\to\pi^0\omega$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\pi^0\omega)$ [pb]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2022_I2047667/d01-x01-y02
Title=Cross section for $e^+e^-\to\eta\omega$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\eta\omega)$ [pb]
ConnectGaps=1
END PLOT
