BEGIN PLOT /MARKII_1990_I304882
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
ConnectGaps=1
LogY=1
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.6$
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0<|\cos\theta|<0.1$
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d02-x01-y02
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.1<|\cos\theta|<0.2$
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d02-x01-y03
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.2<|\cos\theta|<0.3$
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d02-x01-y04
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.3<|\cos\theta|<0.4$
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d02-x01-y05
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.4<|\cos\theta|<0.5$
END PLOT
BEGIN PLOT /MARKII_1990_I304882/d02-x01-y06
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.5<|\cos\theta|<0.6$
END PLOT
