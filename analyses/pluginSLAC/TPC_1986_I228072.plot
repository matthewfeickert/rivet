BEGIN PLOT /TPC_1986_I228072/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /TPC_1986_I228072/d04-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.3$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /TPC_1986_I228072/d05-x01-y01
Title=Cross section for $\gamma\gamma\to K+K^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to K^+K^-)$ [nb]
LogY=1
END PLOT