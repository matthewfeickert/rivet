BEGIN PLOT /HRS_1986_I18688/d01-x01-y01
Title=Scaled momentum for $f_2(1270)$ at 29 GeV
XLabel=$x_p$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1986_I18688/d01-x01-y02
Title=Scaled momentum for $f_0(980)$ at 29 GeV
XLabel=$x_p$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1986_I18688/d01-x01-y03
Title=Scaled momentum for $K_2(1430)^0$ at 29 GeV
XLabel=$x_p$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
