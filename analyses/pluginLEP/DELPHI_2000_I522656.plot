BEGIN PLOT /DELPHI_2000_I522656/d03-
Title=EEC for all  $\cos\theta_T$
XLabel=$\theta$ [degrees]
YLabel=EEC [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d2[1,2,3,4]-
XLabel=$\theta$ [degrees]
YLabel=EEC [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d21-x01-y01
Title=EEC for $0<\cos\theta_T<0.12$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d21-x01-y02
Title=EEC for $0.12<\cos\theta_T<0.24$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d22-x01-y01
Title=EEC for $0.24<\cos\theta_T<0.36$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d22-x01-y02
Title=EEC for $0.36<\cos\theta_T<0.48$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d23-x01-y01
Title=EEC for $0.48<\cos\theta_T<0.60$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d23-x01-y02
Title=EEC for $0.60<\cos\theta_T<0.72$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d24-x01-y01
Title=EEC for $0.72<\cos\theta_T<0.84$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d24-x01-y02
Title=EEC for $0.84<\cos\theta_T<0.96$
END PLOT



BEGIN PLOT /DELPHI_2000_I522656/d04-
Title=AEEC for all  $\cos\theta_T$
XLabel=$\theta$ [degrees]
YLabel=AEEC [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d2[5,6,7,8]-
XLabel=$\theta$ [degrees]
YLabel=AEEC [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d25-x01-y01
Title=AEEC for $0<\cos\theta_T<0.12$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d25-x01-y02
Title=AEEC for $0.12<\cos\theta_T<0.24$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d26-x01-y01
Title=AEEC for $0.24<\cos\theta_T<0.36$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d26-x01-y02
Title=AEEC for $0.36<\cos\theta_T<0.48$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d27-x01-y01
Title=AEEC for $0.48<\cos\theta_T<0.60$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d27-x01-y02
Title=AEEC for $0.60<\cos\theta_T<0.72$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d28-x01-y01
Title=AEEC for $0.72<\cos\theta_T<0.84$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d28-x01-y02
Title=AEEC for $0.84<\cos\theta_T<0.96$
END PLOT

BEGIN PLOT /DELPHI_2000_I522656/d05-
Title=Jet Cone Energy Fraction for all  $\cos\theta_T$
XLabel=$\theta$ [degrees]
YLabel=JCEF [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d3[0,1,2]-
XLabel=$\theta$ [degrees]
YLabel=JCEF [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d29-x01-y01
Title=JCEF for $0<\cos\theta_T<0.12$
XLabel=$\theta$ [degrees]
YLabel=JCEF [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d29-x01-y02
Title=JCEF for $0.12<\cos\theta_T<0.24$
XLabel=$\theta$ [degrees]
YLabel=JCEF [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d30-x01-y01
Title=JCEF for $0.24<\cos\theta_T<0.36$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d30-x01-y02
Title=JCEF for $0.36<\cos\theta_T<0.48$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d31-x01-y01
Title=JCEF for $0.48<\cos\theta_T<0.60$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d31-x01-y02
Title=JCEF for $0.60<\cos\theta_T<0.72$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d32-x01-y01
Title=JCEF for $0.72<\cos\theta_T<0.84$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d32-x01-y02
Title=JCEF for $0.84<\cos\theta_T<0.96$
END PLOT







BEGIN PLOT /DELPHI_2000_I522656/d06-
Title=$1-T$ for all $\cos\theta_T$
XLabel=$1-T$
YLabel=$1/\sigma\text{d}\sigma/\text{d}(1-T)$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d33-
XLabel=$1-T$
YLabel=$1/\sigma\text{d}\sigma/\text{d}(1-T)$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d33-x01-y01
Title=$1-T$ for $0<\cos\theta_T<0.12$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d33-x01-y02
Title=$1-T$ for $0.12<\cos\theta_T<0.24$
END PLOT

BEGIN PLOT /DELPHI_2000_I522656/d07-x01-y01
Title=Oblateness
XLabel=$O$
YLabel=$1/\sigma\text{d}\sigma/\text{d}O$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d08-x01-y01
Title=C Parameter
XLabel=$C$
YLabel=$1/\sigma\text{d}\sigma/\text{d}C$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d09-x01-y01
Title=Heavy Jet Mass
XLabel=$\rho_h$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\rho_h$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d10-x01-y01
Title=Sum of Jet Masses
XLabel=$\rho_\sum$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\rho_\sum$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d11-x01-y01
Title=Difference of Jet Masses
XLabel=$\rho_\text{diff}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\rho_\text{diff}$
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d12-x01-y01
Title=Wide jet broadening
XLabel=$B_W$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_W$
LegendYPos=0.8
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d13-x01-y01
Title=Total jet broadening
XLabel=$B_T$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}B_T$
LegendYPos=0.8
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d17-x01-y01
Title=Differential 2-jet rate(JADE) 
XLabel=$y_{23}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}y_{23}$
LegendYPos=0.8
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d18-x01-y01
Title=Differential 2-jet rate(Durham) 
XLabel=$y_{23}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}y_{23}$
LegendYPos=0.8
END PLOT
BEGIN PLOT /DELPHI_2000_I522656/d20-x01-y01
Title=Differential 2-jet rate(Cambridge) 
XLabel=$y_{23}$
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}y_{23}$
LegendYPos=0.8
END PLOT
