
BEGIN PLOT /ZEUS_1995_I395196/d01-x01-y01
Title=Differential multiplicity of $K^0$ vs. $p_T$
XLabel=$p_T$ [GeV]
YLabel=$1/N_{ev} dN(K^0)/dp^2_T [GeV^2]$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d02-x01-y01
Title=Differential multiplicity of $K^0$ vs. $\eta$ 
XLabel=$\eta$
YLabel=$1/N_{ev} dN(K^0)/d\eta $
LogY=0
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d03-x01-y01
Title=Differential multiplicity of $\Lambda$ vs. $p_T$ 
XLabel=$p_T$ [GeV]
YLabel=$1/N_{ev} dN(\Lambda^0)/dp^2_T [GeV^2]$
END PLOT
 
BEGIN PLOT /ZEUS_1995_I395196/d04-x01-y01
Title=Differential multiplicity of $\Lambda$ vs. $\eta$ 
XLabel=$\eta$
YLabel=$1/N_{ev} dN(\Lambda^0)/d\eta [GeV^2]$
END PLOT
 
BEGIN PLOT /ZEUS_1995_I395196/d05-x01-y01
Title=Mean multiplicity of $K^0$ vs. $Q^2$
LogX=1
ConnectBins=1
XLabel=$Q^2 [GeV^2]$
YLabel=$<K^0 multiplicity>$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d06-x01-y01
Title=Ratio of the mean $K^0$ multiplicity to the mean charged particle multiplicity as a function of the events $Q^2$
LogX=1
XLabel=$Q^2 [GeV^2]$
YLabel=$<N(K^0)>/<N(tracks> $
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d07-x01-y01
Title=Differential $K^0$ multpilicity in NRG events as a function of $p_T$
XLabel=$p_T [GeV]$
YLabel=$1/N_{ev} dN(K^0)/dp^2_T [GeV^2]$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d08-x01-y01
Title=Differential $K^0$ multiplicity in LRG events as a function
 of $p_T$
XLabel=$p_T$ [GeV]
YLabel=$1/N_{ev} dN(K^0)/dp^2_T [GeV^2]$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d09-x01-y01
Title=Differential $K^0$ multpilicity in NRG events as a function
 of $\eta$
XLabel=$\eta$
YLabel=$1/N_{ev} dN(K^0)/d\eta$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d10-x01-y01
Title=Differential $K^0$ multpilicity in LRG events as a function
 of $\eta$
XLabel=$\eta$
YLabel=$1/N_{ev} dN(K^0)/d\eta$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d11-x01-y01
Title=Number of $K^0$ per event as a func. of $Q^2$
XLabel=$Q^2 [GeV^2]$
YLabel=$N(K^0)/event$
END PLOT

BEGIN PLOT /ZEUS_1995_I395196/d12-x01-y01
Title=Number of $\Lambda$ per event as a func. of $Q^2$
XLabel=$Q^2 [GeV^2]$
YLabel=$N(\Lambda)/event$
END PLOT

