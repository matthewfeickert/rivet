BEGIN PLOT /JADE_1984_I221004/d01-x01-y01
Title=$D^{*0}$ scaledenergy
XLabel=$x_E$
YLabel=$s\mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
LogY=0
END PLOT
